import cv2
from datetime import datetime
from emotion_api import *
from microsoftfaceapi3 import *
from multiprocessing import Queue,Process
import boto3
from imutils.video import VideoStream
from imutils import face_utils




def cropFace(faces,frame,name):
    
    image = frame[faces[1] -20:faces[3]+20, faces[0] -20 :faces[2]+20]
    image = imutils.resize(image, width=400)
    cv2.imwrite(name,image)
    return name
def insert_item(table_name, item):
    session = boto3.session.Session()
    dynamodb = session.resource('dynamodb')
    table = dynamodb.Table(table_name)
    response = table.put_item(Item=item)
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        return True
    else:
        return False


def insert_dynamo(url,faces,gender,age):

    tableName = 'advertelligent_engagement'


    for i,u in enumerate(url):
    
        face_data = {"url":u,"age":age[i],"faces_date":faces[i]}
        #final_emotion.append(emotion_data)
        #final_emotion = dict(final_emotion, **emotion_data)

    	#face_parameters = {"ad_url": url,"ad_id":ad_id}
    #engagement_parameters = dict(engagement_parameters, **final_emotion)

    
	    required_hash_data = {
	        "gender": gender[i],
	        "timestamp": str(datetime.now())
	        }
    	item = dict(required_hash_data, **face_data)
    	insert = insert_item(tableName,item)
    	print(insert)
    #return insert
def main():
	hfd = HaarFaceDetector("../etc/xml/haarcascade_frontalface_alt.xml", "../etc/xml/haarcascade_profileface.xml")
	my_face_detector = dlib.get_frontal_face_detector()
	name1 = './emotion_images/'+"person_"+str(person)+"_"+str(datetime.now())+".jpg"
	usingPiCamera = True
	#vs = manager.list()
	frameSize = (640, 480)

	vs = VideoStream(src=0, usePiCamera=usingPiCamera, resolution=frameSize,framerate=10).start()
	while True:
		name = []
		frame = vs.read()
		#frame = imutils.resize(frame, width=500)
		allTheFaces = hfd.returnMultipleFacesPosition(frame, runFrontal=True, runFrontalRotated=True, 
	                            runLeft=False, runRight=False, 
	                            frontalScaleFactor=1.2, rotatedFrontalScaleFactor=1.2, 
	                            leftScaleFactor=1.15, rightScaleFactor=1.15, 
	                            minSizeX=64, minSizeY=64, 
	                            rotationAngleCCW=30, rotationAngleCW=-30)
	            #print("Total Faces: " + str(len(faces_array)))
	    for element in allTheFaces:
	        face_x1 = int(element[0])
	        face_y1 = int(element[1])
	        face_x2 = int(face_x1+element[2])
	        face_y2 = int(face_y1+element[3])
	        text_x1 = face_x1
	        text_y1 = face_y1 - 3
	        #roi_x1 = face_x1 - roi_resize_w
	        
	        

	        #if DEBUG == True:
	        cv2.putText(frame, "FACE " + str(i+1), (text_x1,text_y1), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 1);
	        cv2.rectangle(frame, 
	                     (face_x1, face_y1), 
	                     (face_x2, face_y2), 
	                     (0, 255, 0), 
	                      2)   
	        face_n = [face_x1,face_y1,face_x2,face_y2]
	        name.append(cropFace(face_n,frame,name1))
	    url,faces,gender,age = get_emotion(name)
	    insert_dynamo(url,faces,gender,age)
	vs.stop()
    cv2.destroyAllWindows()

